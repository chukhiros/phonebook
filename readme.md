1. Clone the repo using the command , git clone https://bitbucket.org/chukhiros/phonebook.git  /var/www/hackajob
2. Install Ansible and Vagrant in your development environment.
3. Set the current working directory to /var/www/hackajob/devops/projects and run vagrant up. Vagrant will install ubuntu 16.04,
PHP 7.1, Mysql 5.6 and other dependencies.
4. Run the command vagrant ssh and then set the current working directory to /var/www/hackajob/phonebook.
5. Restart the nginx and mysql server using the command "sudo service nginx/mysql restart "
6. Run composer install 
7. Run the migration using the command "vendor/bin/phinx migrate"
8. To import the contacts from API , run the command "php application/cli.php phonebook default importContacts"
9. Exit the current ssh session.
9. Add the following entry "192.168.17.50   hackajob-phonebook.local" to /etc/hosts.
10. To run the project use the url hackajob-phonebook.local
11. For any problem please contact me at "contactdipak@gmail.com". I will be glad to help you  :D. 