<?php
//@example: php vendor/bin/phalcon.php model --name=<table_name> --directory=<project_dir>
if (! defined('PROJECT_ROOT_PATH')) {
    define("PROJECT_ROOT_PATH", realpath(__DIR__ ));
}

if (! defined('APPLICATION_BASE_PATH')) {
    define('APPLICATION_BASE_PATH', PROJECT_ROOT_PATH . '/application');
}

$namespaces = [
    'Application' => APPLICATION_BASE_PATH,
    'Library' => PROJECT_ROOT_PATH . '/Library/'
];

// Get the application configuration.
$applicationLoader = new \Phalcon\Loader();
$applicationLoader->registerNamespaces($namespaces)->register();
$bootstrap = new \Application\Bootstrap\Web();
$bootstrap->initDI();
$bootstrap->setApplicationBasePath(APPLICATION_BASE_PATH);
$bootstrap->setLoader($applicationLoader);
$bootstrap->setApplicationEnvironment();

$configTemplateIni = "[database]\n%s\n[application]\n%s";

$databaseConfig = "";
$phalconConfig = "";

if(!file_exists("config")){
    mkdir("config");
}
# Creating 
foreach($bootstrap->getConfig()->databases->mysql as $key=>$val){
    $databaseConfig .= sprintf("%s = %s\n",$key,$val);
}

$phalconConfig .= sprintf("modelsDir = %s\n",$bootstrap->getConfig()->application->modelsDir);

file_put_contents("config/config.ini", sprintf($configTemplateIni,$databaseConfig,$phalconConfig));

register_shutdown_function(function(){
   exec("rm -rf config");
});

return [];