<?php
// This is how to run migration. php vendor/bin/phinx migrate
if (! defined('PROJECT_ROOT_PATH')) {
    define("PROJECT_ROOT_PATH", __DIR__);
}

if (! defined('APPLICATION_BASE_PATH')) {
    define('APPLICATION_BASE_PATH', PROJECT_ROOT_PATH . '/application');
}

$namespaces = [
    'Application' => APPLICATION_BASE_PATH,
    'Library' => PROJECT_ROOT_PATH . '/Library/',
    'Phalcon' => PROJECT_ROOT_PATH . "/vendor/phalcon/incubator/Library/Phalcon"
];

// Get the application configuration.
$applicationLoader = new \Phalcon\Loader();
$applicationLoader->registerNamespaces($namespaces)->register();

$cliApplication = new \Application\Bootstrap\Cli();
$cliApplication->initDI();
$cliApplication->setApplicationBasePath(APPLICATION_BASE_PATH);
$phinxEnvIndex = array_search("-e", $_SERVER['argv']);
if (! empty($phinxEnvIndex)) {
    $phinxEnvInfo = array_slice($_SERVER['argv'], $phinxEnvIndex);
    if (count($phinxEnvInfo) > 1) {
        $environment = (string) trim($phinxEnvInfo[1]);
        $cliApplication->setApplicationEnvironment($environment);
    }
} else {
    $cliApplication->setApplicationEnvironment(getenv("PHINX_ENVIRONMENT"));
}


$cliApplication->setLoader();
$config = $cliApplication->getConfig();

$cliApplication->setConfig($config);
$cliApplication->setServiceManager();
$cliApplication->setMysqlDatabase($config->databases->mysql);
$cliApplication->registerModules($config->modules->toArray());

$config = $config->databases->mysql;



$dsn = "mysql:dbname=$config->dbname;host=$config->host";
$dbConxn = new \PDO($dsn, $config->username, $config->password);

return array(
    "paths" => array(
        "migrations" => "application/migrations"
    ),
    "environments" => array(
        "default_migration_table" => "phinxlog",
        "default_database" => $config->dbname,
        "dev" => array(
            "name" => $config->dbname,
            "connection" => $dbConxn,
            "table_prefix" => ""
        ),
        "staging" => array(
            "name" => $config->dbname,
            "connection" => $dbConxn,
            "table_prefix" => ""
        ),
        "production" => array(
            "name" => $config->dbname,
            "connection" => $dbConxn,
            "table_prefix" => ""
        ),
        "testing" => array(
            "name" => $config->dbname,
            "connection" => $dbConxn,
            "table_prefix" => ""
        ),
        "stagingtest" => array(
            "name" => $config->dbname,
            "connection" => $dbConxn,
            "table_prefix" => ""
        )
    )
);