<?php
namespace Library\System\Helpers;

class ArrayHelper
{

    /**
     * Returns all the keys from multidimensional arrays.
     * 
     * @param array $array            
     * @return unknown[]
     */
    static function getRecursiveArrayKeys(array $array)
    {
        $keys = array();
        
        foreach ($array as $key => $value) {
            $keys[] = $key;
            if (is_array($value)) {
                $keys = array_merge($keys, self::getRecursiveArrayKeys($value));
            }
        }
        
        return $keys;
    }

    /**
     * Checks if the two array has identical keys.
     * 
     * @param Array $arr            
     * @param Array $expectedKeys            
     * @return boolean
     */
    static function isArrayKeysValid(array $arr, array $expectedKeys)
    {
        if (count($arr) <= 0 || count($expectedKeys) <= 0) {
            throw new \Library\System\Helpers\ArrayHelperException("Invalid array or expected keys path.");
        }
        
        $arrKeys = self::getRecursiveArrayKeys($arr);
        
        $commonKeys = (array) array_intersect($expectedKeys, $arrKeys);
        
        if ($expectedKeys == $commonKeys) {
            return true;
        }
        
        return false;
    }

    /**
     * Converts array to a given object
     */
    public static function toObject(Array $array, $targetObject)
    {
        if (empty($array) || !is_object($targetObject)) {
            throw new \Library\System\Helpers\ArrayHelperException("Invalid object");
        }
        
        // Get all the properties in a target object.
        $targetObjectReflectionClass = new \ReflectionClass($targetObject);
        $targetObjectProperties = $targetObjectReflectionClass->getProperties(
            \ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED
        );
        $_targetObjectProperties = [];
        
        foreach ($targetObjectProperties as $targetObjectProperty) {
            array_push($_targetObjectProperties, $targetObjectProperty->name);
        }
        
        $targetObjectProperties = $_targetObjectProperties;
        $propertiesToSet = array_intersect(array_keys($array), $targetObjectProperties);
        
        foreach ($propertiesToSet as $property) {
            $propertyName = explode("_", $property);
            $propertyName = array_map('ucfirst', $propertyName);
            array_unshift($propertyName, "set");
            $method = implode("", $propertyName);
            call_user_func_array([
                $targetObject,
                $method
            ], [$array[$property]]);
        }
        
        return $targetObject;
    }
}

class ArrayHelperException extends \Phalcon\Exception
{
}