<?php
namespace Library\System\Controller;
class AbstractController extends \Phalcon\Mvc\Controller
{ 
    public  $response;
    
    protected $_authorizationToken;
    
    public function initialize(){
        $this->request = new \Library\System\Request\Manager();
        $this->response = new \Library\System\Response\Manager();
        $this->loadDefaultAssets();
    }
    
    /**
     * Register default assets listed in the config using phalcon assets manager
     * 
     * @param string $themeType
     */
    public function loadDefaultAssets()
    {
        $assetSettings = $this->getDI()->get("config")->application->themes->default->assets;
        
        $javaScriptBasePath = $assetSettings->js->basepath;
        $javaScriptSources = $assetSettings->js->source;
        
        $styleBasePath = $assetSettings->css->basepath;
        $styleSources = $assetSettings->css->source;
        
        if (count($javaScriptSources) > 0) {
            foreach ($javaScriptSources as $jScript) {
                $this->assets->addJs($javaScriptBasePath . $jScript);
            }
        }
        
        if (count($styleSources) > 0) {
            foreach ($styleSources as $style) {
                $this->assets->addCss($styleBasePath . $style);
            }
        }
    }
}