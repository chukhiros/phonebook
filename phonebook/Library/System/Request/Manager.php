<?php 
namespace Library\System\Request;
class Manager extends \Phalcon\Di\Injectable
{
	/**
	 * This function is created to be able to get 
	 * any request data whether its a json raw data or standard http post request
	 * If the request is standard HTTP GET or POST request, for now the raw Body is empty
	 * and If the request is json then request get or post method is empty . 
	 * If you want a raw data no matter what the request is use the function getRawBody.
	 * @param unknown $rawBody
	 */
	
    /**
     * 
     * @var unknown
     */
	private $__request = NULL;
	
	/**
	 * 
	 * @var unknown
	 */
	private $__rawData = NULL;
	
	public function __construct(){
	   $this->__request = new \Phalcon\Http\Request();
	}
	
	/**
	 * Returns header using php getallheaders function,
	 * @param unknown $header
	 * @return unknown|string
	 */
	public function getHeader($header,$defaultVal=''){
	    // If php is running as apache module
	    if (function_exists('getallheaders')) {
	        $headers = getallheaders();
	    } else {
	        // If php is running as php-fprm , then getallheaders function is not avaialble.
	        // All the headers information is passed thru server global variable ($_SERVER)
	        $headers = array();
	        foreach ($_SERVER as $name => $value) {
	            if (substr($name, 0, 5) == 'HTTP_') {
	                $key = str_replace(' ', '-', (str_replace('_', ' ', substr($name, 5))));
	                $headers[$key] = $value;
	                $header = strtoupper($header);
	            }
	        }
	    }
	    
	    if(array_key_exists($header, $headers)){
	        return $headers[$header];
	    }
	    return $defaultVal;
	}
	
	/**
	 * Returns raw body.
	 * {@inheritDoc}
	 * @see \Phalcon\Http\Request::getRawBody()
	 */
	public function getRawBody(){
	    if($this->__rawData == NULL)
	    {
	        $rawBody = $this->__request->getRawBody(false);
	 
	        if(!is_object(json_decode($rawBody))){
	            $rawBody = urldecode($rawBody);
	            $_rawBody = array();
	            parse_str($rawBody,$_rawBody);
	            $rawBody = json_encode($_rawBody);
	        }
	        
	        $this->__rawData = json_decode($rawBody);
	    }
	
		return $this->__rawData;
	}
	
	/**
	 * Return phalcon request
	 * @return \Phalcon\Http\Request
	 */
	public function getRequest(){
		return $this->__request;
	}
	
	/**
	 * Returns request data.
	 * @param string $data
	 * @param string $defaultValue
	 * @return string
	 */
	
	public function get($data,$defaultValue=''){
	   
		if(count($this->getRawBody()) == 0)
		{
		   return $this->getRequest()->get($data)??$defaultValue; 
		}
		
		
		if(property_exists($this->getRawBody(), $data)){
			return $this->getRawBody()->{$data};
		}
		
		return $defaultValue;
	}

	public function getPost($data,$defaultValue=''){
		if(count($this->getRawBody()) == 0)
		{
                    return $this->getRequest()->getPost($data);
		}
		
		if(property_exists($this->getRawBody(), $data)){
			return $this->getRawBody()->{$data};
		}
		
		return $defaultValue;
	}
        
}
?>