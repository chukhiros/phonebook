<?php
declare(strict_types = 1);
namespace Library\System\Application;

/**
 *
 * @author dkarki
 *        
 */
class Manager extends \Phalcon\Mvc\Application
{

    /**
     *
     * @var Applciation Base Path
     */
    protected $_applicationBasePath;

    /**
     *
     * @var Project root path.
     */
    protected $_projectRootPath;

    /**
     * Sets appplication environment.
     *
     * @param String $applicationEnv            
     * @return \Library\Application\Manager
     */
    public function setApplicationEnvironment($applicationEnv)
    {
        $this->getDI()->set("applicationEnvironment", function () use ($applicationEnv) {
            return $applicationEnv;
        },true);
        
        return $this;
    }

    /**
     *
     * @param unknown $applicationBasePath            
     * @return \Library\Application\Manager
     */
    public function setApplicationBasePath($applicationBasePath)
    {
        $this->_applicationBasePath = $applicationBasePath;
        return $this;
    }

    /**
     *
     * @return unknown
     */
    public function getApplicationBasePath()
    {
        if (empty($this->_applicationBasePath)) {
            throw new \Phalcon\Exception("Application base path undefined.");
        }
        
        return $this->_applicationBasePath;
    }

    /**
     * Sets project root path.
     *
     * @param unknown $projectRootPath            
     * @return \Library\Application\Manager
     */
    public function setProjectRootPath($projectRootPath)
    {
        $this->_projectRootPath = $projectRootPath;
        return $this;
    }

    /**
     * Returns project root path.
     *
     * @return \Library\Project
     */
    public function getProjectRootPath()
    {
        if (empty($this->_projectRootPath)) {
            throw new \Phalcon\Exception("Project root path undefined.");
        }
        
        return $this->_projectRootPath;
    }

    /**
     * Resgisters Modules
     *
     * {@inheritdoc}
     *
     * @param Array $modules            
     * @param
     *            Array | NULL $merge
     *            
     * @see \Phalcon\Mvc\Application::registerModules()
     */
    public function registerModules(Array $modules, $merge = NULL): \Phalcon\Mvc\Application
    {
        $namespaces = (array) $this->getDI()
            ->get("loader")
            ->getNamespaces();
        
        if (count($modules) > 0) {
            foreach ($modules as $moduleName => $module) {
                if ($module['default']) {
                    $this->getDI()
                        ->get('router')
                        ->setDefaultModule($moduleName);
                }
                
                $namespaces = array_merge($namespaces, $module['namespace']);
                unset($module['namespace']);
                
                $modules[$moduleName] = $module;
            }
        }
        
        $this->getDI()
            ->get("loader")
            ->registerNamespaces($namespaces)
            ->register();
        
        parent::registerModules($modules);
        return $this;
    }

    /**
     * Sets Dependency injection container.
     *
     * @param
     *            \Phalcon\Di | NULL $dependencyInjector
     * @return \Phalcon\Mvc\Application
     */
    public function initDI($dependencyInjector = NULL): \Phalcon\Mvc\Application
    {
        if (! $dependencyInjector instanceof \Phalcon\DiInterface) {
            $dependencyInjector = new \Phalcon\Di\FactoryDefault();
        }
        
        $this->setDI($dependencyInjector);
        return $this;
    }

    /**
     * Sets Application Loader
     *
     * @param \Phalcon\Loader\ $loader            
     * @return \Phalcon\Mvc\Application
     */
    public function setLoader($loader = NULL): \Phalcon\Mvc\Application
    {
        if (! $loader instanceof \Phalcon\Loader) {
            $loader = new \Phalcon\Loader();
        }
        
        $this->getDI()->set('loader', $loader);
        return $this;
    }

    /**
     * Get value from Dependency Injection Container.
     *
     * @param String $key            
     * @return mixed
     */
    public function get($key = NULL)
    {
        try {
            return $this->getDI()->get($key);
        } catch (\Phalcon\DI\Exception $e) {
            return "";
        }
    }

    /**
     * registers application setting defined in app/config/ as a shared service
     *
     * @param \Phalcon\config $config            
     */
    public function setConfig(\Phalcon\config $config): \Phalcon\Mvc\Application
    {
        $this->getDI()->set('config', function () use ($config) {
            return $config;
        }, true);
        
        return $this;
    }

    /**
     * Registers application routes in DI container as a shared service.
     *
     * @return \Library\Application\Manager
     */
    public function setRoutes($routes): \Library\System\Application\Manager
    {
        if (count($routes) == 0) {
            return $this;
            // throw new \Phalcon\Application\Exception("Invalid routes.");
        }
        
        $router = new \Phalcon\Mvc\Router();
        $this->getDI()->set('router', function () use ($router, $routes) {
            foreach ($routes as $route) {
                $router->add($route['pattern'], $route['map'], $route['httpMethods']);
            }
            
            return $router;
        });
        
        return $this;
    }

    /**
     * Sets application view.
     *
     * @param
     *            NULL | \Phalcon\MVC\View $view
     */
    public function setView($view = NULL): \Library\System\Application\Manager
    {
        if (! $view instanceof \Phalcon\MVC\View) {
            $view = new \Phalcon\Mvc\View();
        }
        $projectRootPath = $this->getProjectRootPath();
        $this->getDi()->set('view', function () use ($view, $projectRootPath) {
            $view->registerEngines(array(
                ".phtml" => "Phalcon\Mvc\View\Engine\Php"
            ));
            
            $view->setViewsDir($projectRootPath);
            return $view;
        }, true);
        return $this;
    }

    /**
     * Sets Namespaces
     *
     * @param array $namespaces            
     */
    public function setNamespaces($namespaces): \Library\System\Application\Manager
    {
        if (count($namespaces) == 0) {
            throw new \Phalcon\Application\Exception("No namespaces defined.");
        }
        
        $predefinedNamespaces = (array) $this->getDI()
            ->get("loader")
            ->getNamespaces();
        
        $namespaces = array_merge($predefinedNamespaces, $namespaces);
        
        $this->getDI()
            ->get("loader")
            ->registerNamespaces($namespaces)
            ->register();
        
        return $this;
    }

    public function setHttpClient($client)
    {
        $this->getDI()->set("httpClient", function () use ($client) {
            return $client;
        }, true);
        
        return $this;
    }

    public function setLogger($logger = NULL)
    {
        if ($logger == NULL) {
            $logger = new \Library\System\Logger\Adapter();
            $logger = $logger->factory();
        }
        
        $this->getDI()->set("logger", function () use ($logger) {
            return $logger;
        }, true);
        
        return $this;
    }

    /**
     * Sets event manager.
     *
     * {@inheritdoc}
     *
     * @see \Phalcon\Application::setEventsManager()
     */
    public function loadEventsManager($eventsManager = NULL, $eventsListeners = [])
    {
        if (is_array($eventsManager)) {
            throw new \Library\System\Application\Exception("Events manager should be an object or null.");
        }
        
        if ($eventsManager == NULL) {
            $eventsManager = new \Library\System\Event\Manager();
            $eventListenersObj = Array();
            if (count($eventsListeners) > 0) {
                foreach ($eventsListeners as $eventListeners) {
                    $eventType = $eventListeners->eventType;
                    foreach ($eventListeners->listeners as $eventListenerNamespace) {
                        // If object from stated namespace is not created yet, then create one and
                        // save in the event listeners object list, for future use.
                        if (! array_key_exists($eventListenerNamespace, $eventListenersObj)) {
                            $eventListener = new \ReflectionClass($eventListenerNamespace);
                            $eventListener = $eventListener->newInstance();
                            $eventListenersObj[$eventListenerNamespace] = $eventListener;
                        }
                        
                        $eventsManager->attach($eventType, $eventListenersObj[$eventListenerNamespace]);
                    }
                }
            }
        }
        
        $this->getDI()->set('eventsManager', function () use ($eventsManager) {
            return $eventsManager;
        });
        
        return $this;
    }

    /**
     * Sets elasticsearch database.
     */
    public function setElasticsearch()
    {
        $this->getDI()->setShared("elasticsearch", function () {
            $elasticsearchClient = new \Library\System\Elasticsearch\Manager();
            return $elasticsearchClient;
        });
        
        return $this;
    }

    /**
     * Runs application
     *
     * @returns \Phalcon\Http\ResponseInterface
     */
    public function start()
    {
        return $this->handle();
    }

    /**
     * Sets url in dependency injection container.
     */
    public function setUrl(Array $urlConfig)
    {
        $this->getDI()->set("url", function () use ($urlConfig) {
            $url = new \Phalcon\Mvc\Url();
            
            if (array_key_exists("base_uri", $urlConfig)) {
                $url->setBaseUri($urlConfig['base_uri']);
            }
            return $url;
        });
        
        return $this;
    }

    /**
     * Set service manager.
     */
    public function setServiceManager()
    {
        $this->getDI()->set("serviceManager", function () {
            return new \Library\System\Service\Manager();
        });
        return $this;
    }
}
