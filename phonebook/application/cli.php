<?php
if (! defined('PROJECT_ROOT_PATH')) {
    define("PROJECT_ROOT_PATH", realpath(__DIR__ . "/../"));
}
require PROJECT_ROOT_PATH. "/vendor/autoload.php";

if (! defined('APPLICATION_BASE_PATH')) {
    define('APPLICATION_BASE_PATH', PROJECT_ROOT_PATH . '/application');
}

$applicationLoader = new \Phalcon\Loader();
$applicationLoader->registerNamespaces(array(
    'Application' => APPLICATION_BASE_PATH,
    'Library' => PROJECT_ROOT_PATH . '/Library/',
    'Phalcon' => PROJECT_ROOT_PATH . "/vendor/phalcon/incubator/Library/Phalcon"
));

$applicationLoader->register();

$cliApplication = new \Application\Bootstrap\Cli();
$cliApplication->setApplicationBasePath(APPLICATION_BASE_PATH);
$cliApplication->start();