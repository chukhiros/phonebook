<?php
namespace Application\Bootstrap;

/**
 * Bootstrap for web application.
 * It creates service in di using the \Library\Application\Manager.
 *
 * @author dkarki
 * @package Counsellorsclub.
 */
class Web extends \Library\System\Application\Manager
{

    /**
     * Application Environment set in virtualhost.
     *
     * @var String
     */
    protected $_applicationEnv = NULL;

    /**
     * Sets Http client in DI.
     *
     * @return \Application\Bootstrap\Web
     */
    public function setHttpClient($httpClient = NULL)
    {
        $httpClient = new \Phalcon\Http\Client\Provider\Curl();
        $httpClient->setOption(CURLOPT_HEADER, false);
        parent::setHttpClient($httpClient);
        return $this;
    }

    /**
     * Sets application environment in application library.
     *
     * @param unknown $applicationEnv            
     * @throws \Phalcon\Application\Exception
     * @return \Application\Bootstrap\Web
     */
    public function setApplicationEnvironment($applicationEnv = NULL)
    {
        if (! empty($this->_applicationEnv)) {
            return $this;
        }
            
        $this->_applicationEnv = $applicationEnv ?? getenv("APPLICATION_ENV");
        
        if (empty($this->_applicationEnv)) {
            throw new \Phalcon\Application\Exception("Application environment not defined");
        }
        
        parent::setApplicationEnvironment($this->_applicationEnv);
        
        return $this;
    }

    /**
     * Returns merged config of standard and development specific config.
     *
     * @todo : Name of the function needs to be changed.
     * @return \Phalcon\config
     */
    public function getConfig()
    {
     
        if (empty($this->_applicationEnv)) {
            throw new \Library\System\Application\ApplicationException("Undefined application environment");
        }
        
        // Workout the application environment class.
        $applicationEnvClass = preg_replace("/\-.*/", "", strtolower($this->_applicationEnv));
        $applicationEnvClass = ucfirst($applicationEnvClass);
        
        $applicationBasePath = $this->getApplicationBasePath();
        
        // Map configuration file with the configuration classes.
        $classes = [
            "Application\Config\Standard" => $applicationBasePath . "/Config/Standard.php",
            "Application\Config\Dev" => $applicationBasePath . "/Config/" . ucfirst($this->_applicationEnv) . ".php"
        ];
        
        $loader = $this->getDI()->get('loader');
        $loader->registerClasses($classes)->register();
        
        $class = new \ReflectionClass("\Application\Config\\$applicationEnvClass");
        
        return $class->newInstance();
    }
    
    /**
     * Saves Mysql databse connection object in DI container. 
     * Saves transactionManager object in DI container.
     * @param \Phalcon\Config $config
     * @return \Application\Bootstrap\Web
     */
    public function setMysqlDatabase(\Phalcon\Config $config)
    {
        $di = $this->getDI();
        $di->setShared('db', function () use($config,$di){
            $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
                "host" => $config->host,
                "username" => $config->username,
                "password" => $config->password,
                "dbname" => $config->dbname,
                "options" => array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                )
            ));
            $logger = new \Phalcon\Logger\Adapter\File($config->queryLogFile);
            $connectionId = $connection->getConnectionId();
        
            if($config->debug){
                $eventsManager = $di->get('eventsManager');
                $eventsManager->attach("db:beforeQuery",
                    function ($event, $connection) use($logger) {
                        $logger->log($connection->getSQLStatement());
                    }
                );
                $connection->setEventsManager($eventsManager);
            }
            return $connection;
        });
        
        $di->set("transactionManager",function(){
            return new \Phalcon\Mvc\Model\Transaction\Manager();
        });
        
        return $this;
    }

    public function start()
    {
        return parent::start()->getContent();
    }
}