<?php
namespace Application\Bootstrap;

class Cli extends \Application\Bootstrap\Web
{

    /**
     * Starts Cli application.
     */
    public function start()
    {
        $this->initDi(new \Phalcon\Di\FactoryDefault\Cli())
        ->setApplicationEnvironment()
        ->setLoader();
        
        $config = $this->getConfig();
        $modules = $config->modules->toArray();
        $this->setConfig($config);
        $this->setNamespaces($config->namespaces->toArray());
        $this->setHttpClient();
        $this->registerModules($modules);
        $this->loadEventsManager(NULL, $config->eventsListeners);
        $this->setMysqlDatabase($config->databases->mysql);
        //$this->setValidationHelper();
        $this->setServiceManager();
        $this->setLogger();
        $cliApplication = new \Library\System\Application\Cli();
        $cliApplication->registerModules($modules);
        return $cliApplication->setDI($this->getDI())
            ->setConfig($config)
            ->start();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Application\Bootstrap\Web::getConfig()
     */
    public function getConfig()
    {
        $appConfig = parent::getConfig();
        $cliConfig = $appConfig->cli;
        return $appConfig->merge($cliConfig);
    }
}