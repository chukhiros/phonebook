<?php

use Phinx\Migration\AbstractMigration;

class CreateTableContacts extends AbstractMigration
{
    public function up()
    {
        $sql = "
            CREATE TABLE `contacts` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(45) DEFAULT NULL,
              `phone_number` varchar(15) DEFAULT '0',
              `address` varchar(256) DEFAULT '1',
              `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
              `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              FULLTEXT KEY `FTXT_NAME` (`name`,`phone_number`,`address`)
            ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
        ";
        $result = $this->query($sql);
        if(!$result){
            throw new \Phalcon\Db\Exception("There was an error generated while trying to create table contacts");
        }
    }
}
