<?php
namespace Application\Config;

use Phalcon\Config;

class Standard extends \Phalcon\Config
{

    /**
     * Configuration for \Phalcon\Mvc\Url
     *
     * @var array
     */
    const BASE_URI = "";
    
    /**
     * 
     * @var array
     */
    public $url = array(
        'base_uri' => self::BASE_URI
    );

    /**
     * 
     * @var array
     */
    public $classes = Array();

    /**
     * 
     * @var array
     */
    public $eventsListeners = Array();

    /**
     * Event listeners on model activities
     *
     * @var array
     */
    public $modelEventsListeners = Array(
    
    );
    
    /**
     * 
     * @var array
     */
    public $modules = array(
        'phonebook' => array(
            "default" => true,
            "className" => "Application\Modules\Phonebook\Module",
            "path" => APPLICATION_BASE_PATH . "/modules/phonebook/Module.php",
            "defaultNamespaces" => array(
                "webApp" => "Application\\Modules\\Phonebook\\Controllers",
                "cliApp" => "Application\\Modules\\Phonebook\\Tasks"
            ),
            "namespace" => array(
                "Application\\Modules\\Phonebook\\Controllers" => APPLICATION_BASE_PATH . "/modules/phonebook/controllers/",
                "Application\\Modules\\Phonebook\\Tasks" => APPLICATION_BASE_PATH . "/modules/phonebook/tasks/",
                "Application\\Modules\\Phonebook\\Models" => APPLICATION_BASE_PATH . "/modules/phonebook/models/"
            )
        ),
    );

    /**
     * 
     * @var array
     */
    public $namespaces = array(
        'Library' => PROJECT_ROOT_PATH . '/Library/',
        'Application' => APPLICATION_BASE_PATH . '/',
        'Phalcon' => PROJECT_ROOT_PATH . "/vendor/phalcon/incubator/Library/Phalcon"
    );

    /**
     * 
     * @var array
     */
    public $PHPSettings = array(
        'error_reporting' => 0,
        'display_startup_errors' => 0,
        'display_errors' => 0,
        'memory_limit' => "128m",
        'session.auto_start' => 0,
        'date.timezone' => 'Europe/London',
        'max_execution_time' => 600
    );
    
    /**
     * 
     * @var array
     */
    public $routes = array(
        // CMS relevant action
        array(
            'pattern' => self::BASE_URI . 'phonebook/:action',
            'map' => array(
                'module' => 'phonebook',
                'controller' => 'default',
                'action' => '1'
            ),
            'httpMethods' => array(
                'POST',
                'GET'
            )
        )
     );
        
     
    /**
     * 
     * @var array
     */
    public $application = array(
        'name'=>'HackAJobPhoneBook',
        'viewPath' => APPLICATION_BASE_PATH . '/views/',
        'url' => "",
        'secret' => "43eb3f1c8cb3a5",
        'modelsDir' => APPLICATION_BASE_PATH . '/modules/phonebook/models/',
        "apis"=> array(
            "contacts" => "http://www.mocky.io/v2/581335f71000004204abaf83"   
        ),
        'themes' => array (
            "default" => [
                "layoutsDir" => APPLICATION_BASE_PATH . '/themes/default/layouts/',
                "defaultLayout" => "default",
                'assets' => array (
                    'js' => array (
                        'basepath' => '/themes/default/assets/',
                        'source' => array (
                            "js/jquery.min.js",
                            "js/jquery-migrate.min.js",
                             "lib/datatables/datatables.min.js",
                            "js/pages/phonebookv1.js"
                        )
                    ),
                    'css' => array (
                        'basepath' => '/themes/default/assets/',
                        'source' => array (
                            "bootstrap/css/bootstrap.min.css",
                            "css/stylesv1.css?v=123",
                            "css/blue.css"
                        )
                    )
                )
             ]
        )   
    );
    
    /**
     * 
     * @var array
     */
    public $databases = array(
        'mysql' => array(
            'adapter' => "Mysql",
            'host' => '',
            'username' => '',
            'password' => '',
            'dbname' => '',
            'debug' => false,
            'charset' => 'utf8',
            'queryLogFile' => PROJECT_ROOT_PATH . '/logs/sql.log'
        )
    );
    
    /**
     * 
     * @var array
     */
    public $cache = array(
        'frontEnd' => array(
            'lifetime' => 172800
        ),
        'backEnd' => array(
            "cacheDir" => PROJECT_ROOT_PATH . "/cache/objects/"
        )
    );
    
    /**
     * 
     * @var array
     */
    public $logger = array(
        'adapter' => "\Library\System\Logger\Adapter\File",
        'logFile' => PROJECT_ROOT_PATH . '/logs/app.log',
        'logLevel' => \Phalcon\Logger::INFO
    );


    /**
     * 
     * @var array
     */
    public $cli = Array();
    
    /**
     * 
     * @return Array
     */
    public function __construct()
    {
        return parent::__construct(array(
            'application' => $this->application,
            'modules' => $this->modules,
            'namespaces' => $this->namespaces,
            'PHPSettings' => $this->PHPSettings,
            'routes' => $this->routes,
            'databases' => $this->databases,
            'cache' => $this->cache,
            'logger' => $this->logger,
            'classes' => $this->classes,
            'eventsListeners' => $this->eventsListeners,
            'modelEventsListeners' => $this->modelEventsListeners,
            'cli' => $this->cli,
            'url' => $this->url
        ));
    }
}
