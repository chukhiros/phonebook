<?php
namespace Application\Config;

class Dev extends \Application\Config\Standard
{
    public function __construct()
    {
         /**
         *
         * @var \Application\Config\Dev databases
         */
        $this->databases['mysql']['host'] = '192.168.17.50';
        $this->databases['mysql']['username'] = 'hackajobDbUser1';
        $this->databases['mysql']['password'] = 'm3rryChr15tm@52O17';
        $this->databases['mysql']['dbname']="hackajobPhonebook";
        
        return parent::__construct();
    }
}
