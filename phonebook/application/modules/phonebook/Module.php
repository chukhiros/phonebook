<?php
namespace Application\Modules\Phonebook;

class Module extends \Library\System\Module\AbstractModule
{
    public function __construct(){
        $this->setModule("phonebook");
        $this->setModuleDir(__DIR__);
    }
}
