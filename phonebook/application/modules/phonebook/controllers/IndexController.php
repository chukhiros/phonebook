<?php
namespace Application\Modules\Phonebook\Controllers;

class IndexController extends \Library\System\Controller\AbstractController
{
    // List out all the contacts.
    public function indexAction(){
        
    }
    
    /**
     * Returns data in json format.
     * This action is invoked by jquery data table to display the contact details in data grid.
     */
    public function dataAction(){
        $orderByColumns = [
            "0" => "name",
            "1" => "phone_number",
            "2" => "address",
            "3" => "created_at"
        ];
        $orderBy = $orderByColumns[$this->request->get("order")[0]->column];
        $order = $this->request->get("order")[0]->dir;
        $contactModel = new \Application\Modules\Phonebook\Models\Contact();
        $query = $this->request->get("search")->value;
        $contactResultset = $contactModel->find([
           "conditions"=>"name like ?1 or phone_number like ?2 or address like ?3",
            "bind"=>[1=>"%$query%",2=>"%$query%",3=>"%$query%"],
            'order'=>"$orderBy $order"
        ]);
       
        if(count($contactResultset) > 0){
            $contacts = [];
            foreach($contactResultset as $contact){
                array_push($contacts,[
                    $contact->name,
                    $contact->phone_number,
                    $contact->address,
                    $contact->created_at
                ]);
            }
       
            $data = [
                "draw" => 0,
                "recordsTotal"=> $contactModel->count(),
                "recordsFiltered" => count($contacts),
                "data"=>$contacts
            ];
            
            $this->response->setRawData($data);
            $this->response->send();
        }
    }

}