<?php
namespace Application\Modules\Phonebook\Tasks;

class DefaultTask extends \Phalcon\Cli\Task
{   
    const RESPONSE_SUCCESS_STATUS_CODES = [200,201,202,203,204,205,206,207,208,226,300,301,302,303,304,305,306,307,308];
    /*
     * @example php application/cli.php phonebook default importContacts
     */
    public function importContactsAction()
    {
        // Create request object using phalcon curl library.
        $request = new \Phalcon\Http\Client\Provider\Curl();
        
        // Get the contacts api url from configuration.
        $applicationConfig = $this->getDI()->get("config")->application;
        $contactsApi = $applicationConfig->apis->contacts;
        
        // Make a request to the api.
        $response = $request->get($contactsApi);
        
        // Check if the response is valid.
        if($response instanceof \Phalcon\Http\Client\Response){
            
            // Check if the statuscode is 200 OK!.
            $stausCode = $response->header->statusCode;
            if(in_array($stausCode, self::RESPONSE_SUCCESS_STATUS_CODES)){
                // Get the response body.
                $responseBody = $response->body;
                $responseBody = json_decode($responseBody,true);
                if(array_key_exists("contacts", $responseBody)){
                    $contacts = $responseBody["contacts"];
                    foreach($contacts as $contact){
                        $phoneBookModel = new \Application\Modules\Phonebook\Models\Contact();
                        $phoneBookModel = $phoneBookModel->findFirst([
                            "conditions" => "name=?1",
                            "bind" => [1 => $contact["name"]]
                        ]);
                        
                        if($phoneBookModel == NULL){
                            $phoneBookModel = new \Application\Modules\Phonebook\Models\Contact();
                        }
                        
                        $phoneBookModel->name = $contact["name"];
                        $phoneBookModel->phone_number = $contact["phone_number"];
                        $phoneBookModel->address = $contact["address"];
                        $phoneBookModel->save();
                    }
                }
               
            }else{
                throw new \Application\Modules\Phonebook\Tasks\DefaultTaskException(
                    sprintf("\033[31m Cannot complete the request to the api . The status code is %s \033[0m\n",$response->header->statusCode)
                );
            }
        }
    }
}

class DefaultTaskException extends \Phalcon\Exception{}