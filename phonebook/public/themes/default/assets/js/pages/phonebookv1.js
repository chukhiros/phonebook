$(document).ready(function() {
    var dataTable = $('#contacts-grid').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{
            url :"http://hackajob-phonebook.local/index/data",
            type: "post", 
            error: function(){ 
                $("#contacts-grid_processing").css("display","none");
            }
        }
    } );
} );