<?php


define("PROJECT_ROOT_PATH", realpath(__DIR__ . "/../"));
require PROJECT_ROOT_PATH. "/vendor/autoload.php";
define('APPLICATION_BASE_PATH', PROJECT_ROOT_PATH . '/application');

$applicationLoader = new \Phalcon\Loader();
$applicationLoader->registerNamespaces(array(
    'Application' => APPLICATION_BASE_PATH,
    'Library' => PROJECT_ROOT_PATH . '/Library/'
));
$applicationLoader->register();

// Load all the required application dependencies.
$bootstrap = new Application\Bootstrap\Web();
$bootstrap->initDi();
$bootstrap->setLoader($applicationLoader);
$bootstrap->setApplicationBasePath(APPLICATION_BASE_PATH);
$bootstrap->setProjectRootPath(PROJECT_ROOT_PATH);
$bootstrap->setApplicationEnvironment();
$config = $bootstrap->getConfig();
$bootstrap->setConfig($config);
$bootstrap->setNamespaces($config->namespaces->toArray());
$bootstrap->loadEventsManager(NULL, $config->eventsListeners);
$bootstrap->setLogger();
$bootstrap->setRoutes($config->routes->toArray());
$bootstrap->setView();
$bootstrap->setHttpClient();
$bootstrap->registerModules($config->modules->toArray());
$bootstrap->setMysqlDatabase($config->databases->mysql);
$bootstrap->setUrl($config->url->toArray());
$bootstrap->setServiceManager();

try {
    echo $bootstrap->start();
} catch (\Phalcon\Mvc\Dispatcher\Exception $exception) {
    die($exception->getMessage());
}
